function showTab(idx) {
    let tabs = document.getElementsByClassName("tab-button");
    Array.prototype.forEach.call(tabs, (tab) => {
        tab.classList.remove("tab-active");
    });
    tabs[idx].classList.add("tab-active");
    document.getElementById(tabs[idx].getElementsByTagName("a")[0].getAttribute("href").slice(1)).style.display = "block";
}

window.onload = () => {
    showTab(0);

    let tabs = document.getElementsByClassName("tab-button");
    let content = document.getElementsByClassName("profile-tab-content");

    Array.prototype.forEach.call(tabs, (tab) => {
        tab.addEventListener("click", (event) => {
            event.preventDefault();
            Array.prototype.forEach.call(tabs, (tab) => {
                tab.classList.remove("tab-active");
            });
            tab.classList.add("tab-active");

            Array.prototype.forEach.call(content, (el) => {
                el.style.display = "none";
            });
            document.getElementById(tab.getElementsByTagName("a")[0].getAttribute("href").slice(1)).style.display = "block";
        });
    });
};
