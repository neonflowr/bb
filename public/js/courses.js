function find_sub(s, pattern) {
    if (pattern.length === 0) return false;

    let i = 0;
    while(i < s.length) {
        let j = 0;
        while(j < pattern.length) {
            if (s[i] === pattern[j]) {
                if (j === pattern.length - 1) { 
                    return true;
                }
                i++;
                j++;
            }
             else {
                i++;
                break;
            }
        }
    }
    return false;
}

window.onload = () => {
    {
        let postsSearch = document.getElementById("courses-search");

        let input = postsSearch.getElementsByClassName("search-box")[0];
        let btn = postsSearch.getElementsByClassName("search-box-button")[0];

        let courses = document.getElementById("courses").getElementsByClassName("post");

        input.addEventListener("input", (e) => {
            let val = input.value.trim();

            Array.prototype.forEach.call(courses, (el) => {
                let name = el.getElementsByClassName("post-header-link")[0];

                el.classList.remove("search-highlight");

                if (find_sub(name.innerText.toLowerCase(), val.toLowerCase())) {
                    el.classList.add("search-highlight");
                }
            });
        });

        btn.addEventListener("click", (e) => {
            let val = input.value.trim();

            Array.prototype.forEach.call(courses, (el) => {
                let name = el.getElementsByClassName("post-header-link")[0];

                el.classList.remove("search-highlight");

                if (find_sub(name.innerText.toLowerCase(), val.toLowerCase())) {
                    el.classList.add("search-highlight");
                }
            });
        });
    }

    {
        let asignSearch = document.getElementById("assignments-search");

        let input = asignSearch.getElementsByClassName("search-box")[0];
        let btn = asignSearch.getElementsByClassName("search-box-button")[0];

        let courses = document.getElementById("timeline").getElementsByClassName("post");

        input.addEventListener("input", (e) => {
            let val = input.value.trim();

            Array.prototype.forEach.call(courses, (el) => {
                let name = el.getElementsByClassName("post-header-link")[0];

                el.classList.remove("search-highlight");

                if (find_sub(name.innerText.toLowerCase(), val.toLowerCase())) {
                    el.classList.add("search-highlight");
                }
            });
        });

        btn.addEventListener("click", (e) => {
            let val = input.value.trim();

            Array.prototype.forEach.call(courses, (el) => {
                let name = el.getElementsByClassName("post-header-link")[0];

                el.classList.remove("search-highlight");

                if (find_sub(name.innerText.toLowerCase(), val.toLowerCase())) {
                    el.classList.add("search-highlight");
                }
            });
        });
    }

    


};